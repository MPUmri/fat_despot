%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Véronique Fortier 2020 - MIT License 
%
% Fat DESPOT - 2-compartment T1 mapping                                   %

% Implementation based on: Le Ster et al. 2016, JMRI, DOI:10.1002/jmri.25205
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

filesPath_toAdd=strcat(pwd,'/Functions');
addpath(genpath(filesPath_toAdd));

filesPath_toAdd=strcat(pwd,'/Data');
addpath(genpath(filesPath_toAdd));

%% Read DICOM files

% % ** This must be done once for every dataset (each FA)**

% folder='/Users/veroniquefortier/Documents/MR_DATA/20201212_microlipidO2/imaging/FA34_even';
% filename='20201212_microlipidO2_FA34_even.mat';
% sliceOrientation='axial'; % sagittal, coronal or axial
% folderToSave=strcat(pwd,'/Data');
% complexMultiEchoesDataReading(folder, filename,sliceOrientation,folderToSave)
% 
% folder='/Users/veroniquefortier/Documents/MR_DATA/20201212_microlipidO2/imaging/FA34_odd';
% filename='20201212_microlipidO2_FA34_odd.mat';
% complexMultiEchoesDataReading(folder, filename,sliceOrientation,folderToSave)


% % This must also be done for the two B1 scans:

% folder='/Users/veroniquefortier/Documents/MR_DATA/20201212_microlipidO2/imaging/B1_60';
% filename='20201212_microlipidO2_B1_60.mat';
% complexMultiEchoesDataReading(folder, filename,sliceOrientation,folderToSave)

% folder='/Users/veroniquefortier/Documents/MR_DATA/20201212_microlipidO2/imaging/B1_120';
% filename='20201212_microlipidO2_B1_120.mat';
% complexMultiEchoesDataReading(folder, filename,sliceOrientation,folderToSave)


%% Load the data and combine odd and even echoes
nameFiles=...
   ['20201212_microlipidO2_FA3_odd  ';...
    '20201212_microlipidO2_FA6_odd  ';...
    '20201212_microlipidO2_FA15_odd ';...
    '20201212_microlipidO2_FA34_odd ';...
    '20201212_microlipidO2_FA3_even ';...
    '20201212_microlipidO2_FA6_even ';...
    '20201212_microlipidO2_FA15_even';...
    '20201212_microlipidO2_FA34_even'];


clear magn_flyback iField_flyback TE_flyback magn_FA_fw iField_FA_fw
for j=1:(size(nameFiles,1)/2)

    % Load odd echoes
    toRead=nameFiles(j,:);
    toRead= toRead(~isspace(toRead));
    data=load(toRead);
    
    MagnData=double(data.dataScan.magn_rescaleFP);  % magnitude data without Philips scaling
    realIm=double(data.dataScan.real);
    imIm=double(data.dataScan.im);
    PhaseData=double(data.dataScan.phaseReconstructed);     % Phase data reconstructed from real and imaginary

    voxelSize=data.dataScan.voxelSize;
    matrixSize=data.dataScan.matrixSize;
    gamma=267.513*10^6;     %rad/sT
    CF=data.dataScan.CF;
    mainField=2*pi*CF/gamma; 
    TE_all1=data.dataScan.TE;

    if length(TE_all1)>1
        delta_TE=TE_all1(2)-TE_all1(1);
    end
    info=data.dataScan;
    B0_dir=data.dataScan.B0_dir';

    if isempty(PhaseData)==0
        iField = MagnData.*exp(1i*PhaseData);
    end

    % Load even echoes
    toRead=nameFiles(j+size(nameFiles,1)/2,:);
    toRead= toRead(~isspace(toRead));
    data=load(toRead);
    MagnData2(:,:,:,:,j)=double(data.dataScan.magn_rescaleFP);
    realIm2=double(data.dataScan.real);
    imIm2=double(data.dataScan.im);
    PhaseData2=double(data.dataScan.phaseReconstructed);
    TE_all2=data.dataScan.TE;
    iField2 = MagnData2(:,:,:,:,j).*exp(1i*PhaseData2);


    % Combine odd and even echoes
    a=0;
    for i=1:length(TE_all1)
        iField_flyback(:,:,:,i+a)=iField(:,:,:,i);
        iField_flyback(:,:,:,i+a+1)=iField2(:,:,:,i);    
        magn_flyback(:,:,:,i+a)=MagnData(:,:,:,i);
        magn_flyback(:,:,:,i+a+1)=MagnData2(:,:,:,i,j);  
        TE_flyback(i+a)=TE_all1(i);
        TE_flyback(i+a+1)=TE_all2(i);
        a=a+1;
    end

    magn_FA_fw(:,:,:,:,j)=magn_flyback(:,:,:,:);
    iField_FA_fw(:,:,:,:,j)=iField_flyback(:,:,:,:);
end



%% Produce object masks

[ maskSoftTissue,maskObject,maskAirBone] = inVivo_masks(magn_FA_fw(:,:,:,1,1), matrixSize);

maskAirBone=logical(maskAirBone);
maskSoftTissue=logical(maskSoftTissue);
maskObject=logical(maskObject);

figure();montage(reshape4montage(maskSoftTissue),'DisplayRange',[0 1])


%% B1 mapping

data=load('20201212_microlipidO2_B1_60'); %Load B1 scan FA=60
voxelSize=data.dataScan.voxelSize;
matrixSizeB1=data.dataScan.matrixSize;
CF=data.dataScan.CF;
mainField=2*pi*CF/gamma; 
TE=data.dataScan.TE;
alpha=60;   % in degree
MagnData_alpha=double(data.dataScan.magn_rescaleFP);

[ maskObject_B1,maskObjectB1,maskAirBone] = inVivo_masks( MagnData_alpha(:,:,:,1), matrixSizeB1);


data=load('20201212_microlipidO2_B1_120');   %Load B1 scan FA=120
MagnData_2alpha=double(data.dataScan.magn_rescaleFP);

[ B1_map_filtered ] = B1_mapping( MagnData_2alpha, MagnData_alpha, alpha, maskObject_B1 );

%%  Fat water separation

TE_all=TE_flyback;
iField=iField_FA_fw(:,:,:,:,1);


% % FF52
dfat = [0.8586    1.2701    1.5289    2.0025    2.2239    2.7128    4.1554    5.2631];
relAmp = [0.1193    0.4300    0.0678    0.1102    0.0571    0.0510    0.0145    0.1499];
waterFreq=4.7977;

% % FF25
% dfat = [0.8530    1.2614    1.5297    2.0003    2.2355    2.7038    4.1630    5.2570];
% relAmp = [0.1298    0.3962    0.0724    0.1136    0.0542    0.0578    0.0160    0.1600];
% waterFreq=4.8005;

% % FF14
% dfat = [0.8510    1.2628    1.5243    1.9961    2.2380    2.7064 4.2780    5.2672];
% relAmp = [0.1292    0.3594    0.0756    0.1123    0.0526    0.0583    0.0121    0.2006];
% waterFreq=4.8043;


% three-point Dixon - ISMRM toolbox 
algoParams.species(1).name = 'water';
algoParams.species(1).frequency = waterFreq
algoParams.species(1).relAmps = 1;
algoParams.species(2).name = 'fat';
algoParams.species(2).frequency = dfat;
algoParams.species(2).relAmps = relAmp;

algoParams.c1 = 0.2; % Threshold on magnitude weight for seed points play with these when fat/water swap 
algoParams.c2 = 0.75; 


imDataParams.voxelSize=voxelSize;
imDataParams.FieldStrength=mainField;
imDataParams.PrecessionIsClockwise=1;
delta_TE=TE_all(2)-TE_all(1);
imDataParams.images=reshape((iField(:,:,:,1:end)),[matrixSize(1),matrixSize(2),matrixSize(3),1,length(TE_all(1:end))]);
imDataParams.TE=TE_all(1:3);

outParams = fw_i3cm0i_3point_berglund( imDataParams, algoParams);
wfreq=outParams.fieldmap;
wfat=outParams.species(2).amps;
wwater=outParams.species(1).amps;

wfreq = (-wfreq*delta_TE*2*pi); % To account for the sign error in the fat-water toolbox

% Show results
subplot = @(m,n,p) subtightplot (m, n, p, [0.03 0.03], [0.06 0.05], [0.06 0.05]);
figure();subplot(1,3,1);imagesc(wfreq(:,:,ceil(matrixSize(3)/2)));caxis([-pi pi]);colormap gray;axis off;title('Fieldmap [rad]');colorbar;axis off;set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');
subplot(1,3,2);imagesc(abs(wfat(:,:,ceil(matrixSize(3)/2))));colormap gray;axis off;title('Fat image');colorbar;axis off;set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');
subplot(1,3,3);imagesc(abs(wwater(:,:,ceil(matrixSize(3)/2))));colormap gray;axis off;title('Water image');colorbar;axis off;set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');


%% Calculate fat fraction

[ f1, w1 ] = fatQuantification( wfat, wwater, matrixSize );
f1=f1.*maskSoftTissue;
figure();imagesc(f1(:,:,ceil(matrixSize(3)/2)));colormap gray;axis off;title('Fat fraction map / FA=3 - 3point Dixon [%]');colorbar;axis off;set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');caxis([0 100])


%% T2* mapping

weighted_lls=0;
log_lls=1;
[ mapR2Star, mapR2Star_w,residual, weights_wlls_norm] = R2StarMapFast( squeeze(MagnData2(:,:,:,:,1)), squeeze(TE_all2),maskObject(:,:,:),weighted_lls,log_lls );
mapR2Star_w(isnan(mapR2Star_w))=0;
figure();imagesc(mapR2Star_w(:,:,ceil(matrixSize(3)/2)));caxis([0 150]);axis off;title('R_2^* map [s^{-1}]');colormap jet;colorbar;set(gcf,'color','w'); set(gca,'fontsize',20','fontweight','bold');


%% DESPOT1 T1 mapping
% Define which slice should be used for the fit (usually the middles one).
% The number of slice in the B1 scan does not always match the number of
% slice in the MGRE data.
sliceB1=(ceil(matrixSizeB1(3)/2)-1):(ceil(matrixSizeB1(3)/2)+1);
sliceT1=(ceil(matrixSize(3)/2)-1):(ceil(matrixSize(3)/2)+1);

FA=([3,6,15,34]);   % in degree
TR=18/1000;     % in s

B1_map=B1_map_filtered(:,:,sliceB1);
matrixSize=size(B1_map);

maskT1=maskSoftTissue(:,:,sliceT1);

% Scale VFA with B1 correction
clear mapFA_scale mapFA
for i=1:length(FA)
    mapFA(:,:,:,i)=ones(matrixSize(1),matrixSize(2),matrixSize(3)).*FA(i).*maskT1;
    mapFA_scale(:,:,:,i)=mapFA(:,:,:,i).*(B1_map);
end

[ T1_map_B1corr, M0_map ] = t1Mapping_VFA( squeeze(magn_FA_fw(:,:,sliceT1,1,:)), maskT1, TR,degtorad(FA),matrixSize,degtorad(mapFA_scale));
T1_map_B1corr(isnan(T1_map_B1corr))=0;
figure();imagesc((T1_map_B1corr(:,:,1)*1000));colormap parula;colorbar;caxis([0 1000]);axis off;set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');title('VFA T_1 map [ms]')


%% Fat DESPOT joint fit
% Define the fat spectrum (this should ideally be measured with MRS for
% every experiments

% % % FF52
dfat = ([0.8586    1.2701    1.5289    2.0025    2.2239    2.7128    4.1554    5.2631]-4.7977)*1e-6*CF;
relAmp = [0.1193    0.4300    0.0678    0.1102    0.0571    0.0510    0.0145    0.1499];

% % % FF25
% dfat = ([0.8530    1.2614    1.5297    2.0003    2.2355    2.7038    4.1630    5.2570]-4.8005)*1e-6*CF;
% relAmp = [0.1298    0.3962    0.0724    0.1136    0.0542    0.0578    0.0160    0.1600];

% % % FF14
% dfat = ([0.8510    1.2628    1.5243    1.9961    2.2380    2.7064    4.2780    5.2672]-4.8043)*1e-6*CF;
% relAmp = [0.1292    0.3594    0.0756    0.1123    0.0526    0.0583    0.0121    0.2006];
 


% ISMRM toolbox berglund spectrum:
% dfat = ([0.90, 1.30, 1.60, 2.02, 2.24, 2.75, 4.20, 5.19, 5.29]-4.77)*1e-6*CF;
% relAmp = [88 642 58 62 58 6 39 10 37]/1000;

% Bone marrow spectrum:
% dfat = ([0.8890    1.3008    1.5700    2.0200    2.2426    2.7683    4.2200    5.3198 ]-4.6549)*1e-6*CF;
% relAmp = [0.0869    0.5913    0.0613    0.0784    0.0657    0.0183    0.0221    0.0761];


% Joint fat-water T1 fit
[ fat_frac, t1w_2, t1f_2, r_squared,  residual] = fatDESPOT( magn_FA_fw(:,:,sliceT1,:,:), TE_flyback, TR, FA,dfat,relAmp,B1_map, maskT1,f1(:,:,sliceT1),mapR2Star_w(:,:,sliceT1),T1_map_B1corr, TE_all1,TE_all2);







