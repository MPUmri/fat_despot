%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Véronique Fortier 2020 - MIT License 
%
% Fat DESPOT T1 simulations                                               %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filesPath_toAdd=strcat(pwd,'/Functions');
addpath(genpath(filesPath_toAdd));

filesPath_toAdd=strcat(pwd,'/Data');
addpath(genpath(filesPath_toAdd));

%% Numerical phantom
matrixSize=50;
stepSize=(99.99-2)/(matrixSize-1);

fatFraction=[0.01:stepSize:99.99]/100;
fatFraction=repmat(fatFraction,[matrixSize,1]);

fatFraction_reshape=fatFraction(:);   

T1f=(0.6679*(fatFraction_reshape*100).^(-0.6547)+0.1678)+0.1;
R1f=1./T1f;

% Multiple fat T1 varies as a function of fat fraction (based on STEAM MRS
% measurements)
T1f_1=0.883*(fatFraction_reshape*100).^(-0.28);
R1f_1=1./T1f_1;
T1f_2=0.536*(fatFraction_reshape*100).^(-0.23);
R1f_2=1./T1f_2;
T1f_3=1.329*(fatFraction_reshape*100).^(-0.48);
R1f_3=1./T1f_3;
T1f_4=1.215*(fatFraction_reshape*100).^(-0.39);
R1f_4=1./T1f_4;
T1f_5=1.475*(fatFraction_reshape*100).^(-0.53);
R1f_5=1./T1f_5;
T1f_6=1.029*(fatFraction_reshape*100).^(-0.20);
R1f_6=1./T1f_6;
T1f_7=0.475*(fatFraction_reshape*100).^(-0.14);
R1f_7=1./T1f_7;
T1f_8=0.983*(fatFraction_reshape*100).^(-0.23);
R1f_8=1./T1f_8;

% Water T1 varies as a function of fat fraction (based on STEAM MRS
% measurements)
T1w=(-0.0076*(fatFraction_reshape*100)+0.804)+0.75;
R1w=1./T1w;
TR=0.018;

% Define flip angles (an error on flip angle can be simulated with the parameter error)
error=1; % 1= no error, >1 = overestimation, <1=underestimation
flipAngle=degtorad([error*3,error*6, error*15, error*34]);
flipAngle_theoretical=degtorad([3,6,15,34]);

% Define TEs, S0 and R2*
echoTimes=[1.5, 2.7, 3.9, 5.1, 6.3, 7.5]./1000;
TE_reshape_vector=repmat(echoTimes',[length(flipAngle_theoretical),1]);

S0f=1000;
S0w=100;
S0_vector=S0w.*fatFraction_reshape+S0f.*(1-fatFraction_reshape);
T2star_f=0.01;
T2star_w=0.035;
R2star_f=1/T2star_f;
R2star_w=1/T2star_w;
R2star_apparent=R2star_f.*fatFraction_reshape+R2star_w.*(1-fatFraction_reshape);
R2star_apparent_shape=R2star_apparent(:);
CF=127765509;

% Define fat spectrum at different TMs (Tm=0 corresponding to the true fat
% spectrum, TM=16 or 50 corresponding to the experimentally measured fat spectrum)
dfat = ([0.9, 1.31, 1.61, 2.0, 2.26, 2.76, 4.23, 5.27]-4.83)*1e-6*CF;
relAmp_TM0 = [12.96,48.87,8.05,5.75,7.16,1.63,2.32,13.26]/100;
relAmp_TM16 = [13.03,47.84,8.30,5.63,7.10,1.62,2.57,13.89]/100;
relAmp_TM50 = [13.66,47.77,8.01,5.71,6.79,1.69,2.08,14.28]/100;
relAmp=relAmp_TM0;


%% Signal calculation (magnitude only)

clear mapFA fatSpectrum_sin fatSpectrum_cos signal_sim

for i=1:length(flipAngle)
    mapFA(:,i)=ones(length(echoTimes),1).*flipAngle(i);
end
mapFA_scale_vector=mapFA(:);

% --- Multiple fat T1 (cos and sing terms)
fatSpectrum_cos_m=zeros(length(echoTimes),1);
for j=1:length(dfat) 
    for i=1:length(echoTimes)
        c=0;  
        c = c+(relAmp(j)*cos(2*pi*echoTimes(i)*dfat(j)));
        fatSpectrum_cos_m(i,j) = (c);
    end
end
fatSpectrum_cos_m=repmat(fatSpectrum_cos_m,[length(flipAngle),1]);

fatSpectrum_sin_m=zeros(length(echoTimes),1);
for j=1:length(dfat) 
    for i=1:length(echoTimes)
        c=0;
        c = c+(relAmp(j)*sin(2*pi*echoTimes(i)*dfat(j)));
        fatSpectrum_sin_m(i,j) = (c);
    end
end
fatSpectrum_sin_m=repmat(fatSpectrum_sin_m,[length(flipAngle),1]);

 
% Signal model
for i=1:length(fatFraction_reshape)

        signal_sim_multipleFatT1(:,i)=(S0_vector(i).*exp(-TE_reshape_vector.*R2star_apparent(i)).*...
        sqrt(((1-fatFraction_reshape(i)).*((((1-exp(-TR.*R1w(i))).*sin(mapFA_scale_vector))./...
        (1-exp(-TR.*R1w(i)).*cos(mapFA_scale_vector))))+...
        ((fatFraction_reshape(i)).*(((((1-exp(-TR.*R1f_1(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_1(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_cos_m(:,1)+((((1-exp(-TR.*R1f_2(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_2(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_cos_m(:,2)+((((1-exp(-TR.*R1f_3(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_3(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_cos_m(:,3)+((((1-exp(-TR.*R1f_4(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_4(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_cos_m(:,4)+((((1-exp(-TR.*R1f_5(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_5(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_cos_m(:,5)+((((1-exp(-TR.*R1f_6(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_6(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_cos_m(:,6)+((((1-exp(-TR.*R1f_7(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_7(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_cos_m(:,7)+((((1-exp(-TR.*R1f_8(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_8(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_cos_m(:,8)))).^2+(...
        ((fatFraction_reshape(i)).*(((((1-exp(-TR.*R1f_1(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_1(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_sin_m(:,1)+((((1-exp(-TR.*R1f_2(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_2(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_sin_m(:,2)+((((1-exp(-TR.*R1f_3(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_3(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_sin_m(:,3)+((((1-exp(-TR.*R1f_4(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_4(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_sin_m(:,4)+((((1-exp(-TR.*R1f_5(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_5(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_sin_m(:,5)+((((1-exp(-TR.*R1f_6(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_6(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_sin_m(:,6)+((((1-exp(-TR.*R1f_7(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_7(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_sin_m(:,7)+((((1-exp(-TR.*R1f_8(i))).*sin(mapFA_scale_vector))./(1-exp(-TR.*R1f_8(i)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_sin_m(:,8)))).^2));
    

end

%% Add noise to simulation
addNoise=1;
SNR=5:stepSize:60;
stepSize=(max(SNR)-min(SNR))/(matrixSize-1);

noiseMatrix=zeros(matrixSize,matrixSize);
clear noiseToAdd out
if addNoise==1
    repetitions=100;
    for k=1:repetitions

        for j=1:(length(echoTimes)*length(flipAngle))
            for i=1:length(SNR) 
                in=reshape(signal_sim_multipleFatT1(j,:),[matrixSize,matrixSize]);
                out(i,:)=awgn(in(i,:)',SNR(i),'measured');
            end
            signal_sim_multipleFatT1_noise(j,:,k)=reshape(out,[1,matrixSize*matrixSize]);
        end

    end
    
end




%% Fat DESPOT fit 
for i=1:length(flipAngle_theoretical)
    mapFA_th(:,i)=ones(length(echoTimes),1).*flipAngle_theoretical(i);
end

mapFA_scale_vector=mapFA_th(:);

relAmp_fit=relAmp_TM16;

fatSpectrum_cos=zeros(length(echoTimes),1);
for i=1:length(echoTimes)
    c=0;
    for j=1:length(dfat)   
        c = c+(relAmp_fit(j)*cos(2*pi*echoTimes(i)*dfat(j)));
    end
    fatSpectrum_cos(i) = (c);
end
fatSpectrum_cos=repmat(fatSpectrum_cos,[length(flipAngle_theoretical),1]);

fatSpectrum_sin=zeros(length(echoTimes),1);
for i=1:length(echoTimes)
    c=0;
    for j=1:length(dfat)   
        c = c+(relAmp_fit(j)*sin(2*pi*echoTimes(i)*dfat(j)));
    end
    fatSpectrum_sin(i) = (c);
end
fatSpectrum_sin=repmat(fatSpectrum_sin,[length(flipAngle_theoretical),1]);


tic
clear x residual resnorm
h=waitbar(0,'Please wait...');
steps=length(fatFraction_reshape); 

for i=1:repetitions

    magn_FA_fw_masked=signal_sim_multipleFatT1_noise(:,:,i)';


    for k=1:steps

        magn_FA_fw_reshape=magn_FA_fw_masked(k,:);
        magn_FA_fw_reshape_vector=magn_FA_fw_reshape(:);


        signalModeling_magnitude=@(y) magn_FA_fw_reshape_vector - (y(1).*...
        sqrt(((1-y(4)/100).*exp(-TE_reshape_vector.*y(5)).*((((1-exp(-TR.*y(2))).*sin(mapFA_scale_vector))./...
        (1-exp(-TR.*y(2)) .*cos(mapFA_scale_vector))))+...
        ((y(4)/100).*exp(-TE_reshape_vector.*y(5)).*((((1-exp(-TR.*y(3))).*sin(mapFA_scale_vector))./(1-exp(-TR.*y(3)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_cos)).^2+(...
        ((y(4)/100).*exp(-TE_reshape_vector.*y(5)).*((((1-exp(-TR.*y(3))).*sin(mapFA_scale_vector))./(1-exp(-TR.*y(3)).*...
        cos(mapFA_scale_vector)))).*fatSpectrum_sin)).^2));

        x0=ones(1,5);
        lb=ones(1,5);
        ub=ones(1,5);

        x0(1,1)=max(magn_FA_fw_reshape_vector(:))*10;
        x0(1,3)=4;

        x0(1,2)=1;

        if fatFraction_reshape(k)>0.5
            x0(1,4)=0.8*(fatFraction_reshape(k)*100);
        else
            x0(1,4)=1.2*(fatFraction_reshape(k)*100);
        end

        x0(1,5)=R2star_apparent(k);

        lb(1,1)=0.00001;
        lb(1,2)=1/3;
        lb(1,3)=1/0.8;
        lb(1,4)=0.01;
        lb(1,5)=0.000010;

        ub(1,1)=1*10^(15);
        ub(1,2)=1/0.4;
        ub(1,3)=1/0.1;
        ub(1,4)=99.99;
        ub(1,5)=800;

        options_magnitude = optimoptions(@lsqnonlin,'Algorithm','trust-region-reflective','display','off');
        [x(k,:,i),resnorm(k,:,i),residual(:,k,i) ]= lsqnonlin(signalModeling_magnitude,x0,lb,ub,options_magnitude);

        
    end
    
    waitbar(i/repetitions)


end
toc  

close(h)

%% Plot the results
x_averaged=mean(x,3);

fat_frac=(x_averaged(:,4));
fat_frac=reshape(fat_frac,[matrixSize,matrixSize]);
figure();imagesc((fat_frac));colorbar;colormap gray;axis off;caxis([0 100]);title('Fat fraction fitted [%]');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

figure();imagesc((fatFraction*100));colorbar;colormap gray;axis off;caxis([0 100]);title('Fat fraction [%]');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

difference_ff=fat_frac-fatFraction*100;
figure();imagesc((difference_ff));colorbar;colormap jet;axis off;caxis([-10 10]);title('Fat fraction error [%]');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

ff_std=x(:,4,:);
fat_frac_std=reshape(ff_std,[matrixSize,matrixSize,repetitions]);
difference_ff_std=fat_frac_std-fatFraction*100;


t1w_std=1./x(:,2,:);
t1w_std=reshape(t1w_std,[matrixSize,matrixSize,repetitions]);
t1w=1./(x(:,2));
t1w(isnan(t1w))=0;
t1w=reshape(t1w,[matrixSize,matrixSize]);
figure();imagesc((t1w.*1000));colorbar;colormap gray;axis off;title('Water T_1 [ms]');caxis([0 1500]);set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

t1f_std=1./x(:,3,:);
t1f_std=reshape(t1f_std,[matrixSize,matrixSize,repetitions]);
t1f=1./(x(:,3));
t1f(isnan(t1f))=0;
t1f=reshape(t1f,[matrixSize,matrixSize]);
figure();imagesc((t1f.*1000));colorbar;colormap gray;axis off;title('Fat T_1 [ms]');caxis([0 500]);set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');


T1w_r=reshape(T1w,[matrixSize matrixSize]);
difference_T1w=T1w_r*1000-t1w*1000;
figure();imagesc((difference_T1w));colorbar;colormap jet;axis off;caxis([-30 30]);title('Water T_1 error [ms]');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');
difference_T1w_percent=difference_T1w./(T1w_r*1000)*100;
figure();imagesc((difference_T1w_percent));colorbar;colormap jet;axis off;caxis([-40 40]);title('Water T_1 error [%]');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');


r2star2=(x_averaged(:,5));
r2star2=reshape(r2star2,[matrixSize,matrixSize]);
figure();imagesc((r2star2));colorbar;colormap gray;axis off;caxis([0 200]);title('R_2* fitted [s^{-1}]');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');


