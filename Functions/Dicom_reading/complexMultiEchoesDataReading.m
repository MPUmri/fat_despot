%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Véronique Fortier 2020 - MIT License
%
% Multi echoes complex data reading from philips ingenia 3T scanner       %
% Read one sequence at the time, work with classic or enhanced Dicom      %
%                                                                         %
% ----------------------------------------------------------------------- %
% Inputs:                                                                 %
% folder is the data folder, filename is the name of the file for the     %
% structure to be saved, sliceOrientation is the orientation of the slice %
% during the data acquisition (coronal, axial or sagittal), folderToSave
% is the folder where to save the .mat file
% ----------------------------------------------------------------------- %
% Output:                                                                 %
% The structure saved contains the matrix size, the voxel size, the TEs,  %
% the magnitude data (4D matrix, 4th dimension is echo time),the phase    %
% data (4D matrix, 4th dimension is echo time), the central frequency     %
% and more (reconstructed magnitude, phase, rescale magnitude, real and im%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Example how to call this function and access the information
% folder='/Users/veroniquefortier/Desktop/Mrdev1588321111_5echoes_maxBW_minTE_sagittal'; 
% filename='dataScanVolunteer1_sagittal.mat';
% sliceOrientation='axial';
% complexMultiEchoesDataReading(folder, filename, sliceOrientation) % Read the data and save the structure to a .mat file
% 
% data=load('dataScanVolunteer1_sagittal'); % Load the data
% 
% MagnData=data.dataScan.magnitude;
% PhaseData=data.dataScan.phase;
% voxelSize=data.dataScan.voxelSize;
% matrixSize=data.dataScan.matrixSize;
% CF=data.dataScan.CF;
% mainField=2*pi*CF/gamma; 
% TE_all=data.dataScan.TE;
% B0_dir=data.dataScan.B0_dir;

%%

function [  ] = complexMultiEchoesDataReading(folder, filename,sliceOrientation,folderToSave)
%%

% Read the files in the folder
folder1=strcat(folder,'/');
filelist = dir(folder1);
folderList=[1];

% Get of list of all the folders
j=2;
for i=1:length(filelist)
    if filelist(i).isdir==1
        nameFolder=filelist(i).name;
        if nameFolder(1)~='.'
            folderList(j)=filelist(i);
            folderList(j).name=strcat(folderList(j).name,'/');
            j=j+1;
        end
    end
end

NumEcho=0;
minSlice = 1e10;
maxSlice = -1e10;
maxloc=[];
k=1;
classicDicom=0;
enhancedDicom=0;
imageType=0;
t=1;
toWrite=100000;
flag_classic='z';

% Read data in each folder
for n=1:length(folderList)
    if length(folderList)~=1
        folder2=strcat(folder1,folderList(n).name);
    else
        folder2=folder1;
    end
    filelist = dir(folder2);  
    
    i=1;
    while i<=length(filelist)
        if filelist(i).isdir==1
            filelist = filelist([1:i-1 i+1:end]);   % skip folders
        else
            i=i+1;
        end
    end


    for m = 1:(length(filelist))
        nameFold=filelist(m).name;
        if nameFold(1)=='I'
            info = dicominfo([folder2 filelist(m).name]);
            if (isfield(info,'AcquisitionDate')==1)
                classicDicom=1;
                    data(:,:,k)  = single(dicomread([folder2 filelist(m).name]));  
                    if info.SliceLocation<minSlice
                        minSlice = info.SliceLocation;
                        minLoc = info.ImagePositionPatient;
                    end
                    
                    if info.SliceLocation>maxSlice
                        maxSlice = info.SliceLocation;
                        maxLoc = info.ImagePositionPatient;
                    end
                    
                    if info.EchoNumber>NumEcho              % List the echo times
                        NumEcho = info.EchoNumber;      % For Matlab versions after 2022, use info.EchoNumbers
                        TE(info.EchoNumber)=info.EchoTime*1e-3;
                        slope(info.EchoNumber)=info.RescaleSlope;
                        intercept(info.EchoNumber)=info.RescaleIntercept;
                    end
                    
                    toWrite=info.Private_2005_1011;
                    if toWrite~=flag_classic
                        flag_classic=info.Private_2005_1011;
                            imageType(t)=toWrite;
                            slope(t)=info.RescaleSlope;
                            intercept(t)=info.RescaleIntercept;
                            scale_slope(t)=info.Private_2005_100e;
                            scale_intercept(t)=info.Private_2005_100d;
                            t=t+1;
                    end
                    
                    if k==1
                        matrixSize(1) = double(info.Width);
                        matrixSize(2) = double(info.Height);
                        voxelSize(1,1) = info.PixelSpacing(1);
                        voxelSize(2,1) = info.PixelSpacing(1);
                        voxelSize(3,1) = info.SliceThickness;
                        CF = info.ImagingFrequency *1e6;
                        TI=info.InversionTime;
                    end
                    k=k+1;
            end
            
            
            if (isfield(info,'AcquisitionDateTime')==1)
                enhancedDicom=1;
                    data = single(dicomread([folder2 filelist(m).name]));  
                    data=permute(data,[1,2,4,3]);
                    frames=info.PerFrameFunctionalGroupsSequence;
                    frameName0='Item_';
                    for j=1:size(data,3)
                        frameName=strcat(frameName0,num2str(j));
                        info2=frames.(frameName).Private_2005_140f.Item_1;
                        info_loc=frames.(frameName).PlanePositionSequence.Item_1.ImagePositionPatient;

                        if (strcmp(sliceOrientation,'sagittal')==1)
                            indiceSlice=1;
                        elseif (strcmp(sliceOrientation,'axial')==1) 
                            indiceSlice=3;
                        else
                            indiceSlice=2;
                        end
                        if info_loc(indiceSlice)<minSlice
                            minSlice = info_loc(indiceSlice);
                            minLoc = info_loc;
                        end
                        if info_loc(indiceSlice)>maxSlice
                            maxSlice = info_loc(indiceSlice);
                            maxLoc = info_loc;
                        end
                        
                        
                        
                        if info2.EchoNumber>NumEcho              % List the echo times
                            NumEcho = info2.EchoNumber;          % For Matlab versions after 2022, use info2.EchoNumbers
                            TE(info2.EchoNumber)=info2.EchoTime*1e-3;
                        end

                        if info2.Private_2005_1011=='IP'
                                now=1;
                        elseif info2.Private_2005_1011=='OP'
                                now=0;
                        else
                                now=info2.Private_2005_1011;
                        end

                        if now~=toWrite;
                            if info2.Private_2005_1011=='IP'
                                toWrite=1;
                            elseif info2.Private_2005_1011=='OP'
                                toWrite=0;
                            else
                                toWrite=info2.Private_2005_1011;
                            end

                            imageType(t)=toWrite;
                            slope(t)=info2.RescaleSlope;
                            intercept(t)=info2.RescaleIntercept;
                            scale_slope(t)=info2.Private_2005_100e;
                            scale_intercept(t)=info2.Private_2005_100d;
                            t=t+1;
                        end
                        
                        if j==1
                            matrixSize(1) = double(info.Width);
                            matrixSize(2) = double(info.Height);
                            voxelSize(1,1) = info2.PixelSpacing(1);
                            voxelSize(2,1) = info2.PixelSpacing(1);
                            voxelSize(3,1) = info2.SliceThickness;
                            CF = info2.ImagingFrequency *1e6;
                            TI=info2.InversionTime;
                        end
                    end

            end
        end
    end
end

%%
matrixSize(3) = round(norm(maxLoc - minLoc)/voxelSize(3)) + 1  
% matrixSize(3) = round(norm(maxLoc - minLoc)/voxelSize(3)) - 1  
% matrixSize(3) = round(norm(maxLoc - minLoc)/voxelSize(3))

if enhancedDicom==1
    Affine2D = reshape(info.PerFrameFunctionalGroupsSequence.(frameName).PlaneOrientationSequence.Item_1.ImageOrientationPatient,[3 2]);
else
    Affine2D = reshape(info.ImageOrientationPatient,[3 2]);
end

% Main field orientation
Affine3D = [Affine2D (maxLoc-minLoc)/( (matrixSize(3)-1)*voxelSize(3))];
B0_dir =Affine3D\[0 0 1]'; 
B0_dir=round(B0_dir);

% Classify the data
numberEchoes=length(TE);
magnitude=[];
phase=[];
numSlices=matrixSize(3);
realIm=[];
imIm=[];
fatIm=[];
waterIm=[];
OPIm=[];
IPIm=[];
indexP=find(imageType==80); % =='P'
indexIm=find(imageType==73); % =='I'
indexReal=find(imageType==82); % =='R'
indexM=find(imageType==77); % =='M'
indexF=find(imageType==70); % =='F'
indexIP=find(imageType==1); % =='IP'
indexOP=find(imageType==0); % =='OP'
indexW=find(imageType==87); % =='W'

for i=0:numberEchoes-1
    if isempty(indexM)~=1
        magnitude=cat(4,magnitude,data(:,:,(numSlices*numberEchoes*(indexM-1)+1+i)+(1-1)*numSlices*numberEchoes:numberEchoes:indexM*numSlices*numberEchoes+(1-1)*numSlices*numberEchoes));
    end
    if isempty(indexP)~=1   
        phase=single(cat(4,phase,data(:,:,(numSlices*numberEchoes*(indexP-1)+1+i)+(1-1)*numSlices*numberEchoes:numberEchoes:indexP*numSlices*numberEchoes+(1-1)*numSlices*numberEchoes)));
    end
    if isempty(indexReal)~=1    
        realIm=single(cat(4,realIm,data(:,:,(numSlices*numberEchoes*(indexReal-1)+1+i)+(1-1)*numSlices*numberEchoes:numberEchoes:indexReal*numSlices*numberEchoes+(1-1)*numSlices*numberEchoes)));
    end
    if isempty(indexIm)~=1    
        imIm=single(cat(4,imIm,data(:,:,(numSlices*numberEchoes*(indexIm-1)+1+i)+(1-1)*numSlices*numberEchoes:numberEchoes:indexIm*numSlices*numberEchoes+(1-1)*numSlices*numberEchoes)));
    end
    
    if isempty(indexF)~=1
        fatIm=cat(4,fatIm,data(:,:,(numSlices*numberEchoes*(indexF-1)+1+i)+(1-1)*numSlices*numberEchoes:numberEchoes:indexF*numSlices*numberEchoes+(1-1)*numSlices*numberEchoes));
    end
    if isempty(indexIP)~=1   
        IPIm=single(cat(4,IPIm,data(:,:,(numSlices*numberEchoes*(indexIP-1)+1+i)+(1-1)*numSlices*numberEchoes:numberEchoes:indexIP*numSlices*numberEchoes+(1-1)*numSlices*numberEchoes)));
    end
    if isempty(indexOP)~=1    
        OPIm=single(cat(4,OPIm,data(:,:,(numSlices*numberEchoes*(indexOP-1)+1+i)+(1-1)*numSlices*numberEchoes:numberEchoes:indexOP*numSlices*numberEchoes+(1-1)*numSlices*numberEchoes)));
    end
    if isempty(indexW)~=1    
        waterIm=single(cat(4,waterIm,data(:,:,(numSlices*numberEchoes*(indexW-1)+1+i)+(1-1)*numSlices*numberEchoes:numberEchoes:indexW*numSlices*numberEchoes+(1-1)*numSlices*numberEchoes)));
    end
end

% scale magnitude with scale_slope/scale_intercept (philips rescale
% images)- this is important if data from different sequences have to be
% fitted (intercept should be 0 for magnitude) - this takes values from SV
% (matlab) to FP
magnitude_rescaleFP=(magnitude-scale_intercept(indexM))/scale_slope(indexM);
phase_rescaleFP=(phase-scale_intercept(indexP))/scale_slope(indexP);
real_rescaleFP=(realIm-scale_intercept(indexReal))/scale_slope(indexReal);
im_rescaleFP=(imIm-scale_intercept(indexIm))/scale_slope(indexIm);


% Scale the phase between -pi and pi (this take de values from SV to
% DV (see Philips slide on scaling)
phase_rescale=[];
if isempty(indexP)~=1 
    phase_rescale=0.001*(phase*slope(indexP)+intercept(indexP));   
    real_rescale=realIm*slope(indexReal)+intercept(indexReal);
    im_rescale=imIm*slope(indexIm)+intercept(indexIm);
    magnitude_rescaleDV=magnitude*slope(indexM)+intercept(indexM);
end

phaseReconstructed=[];
magnitudeReconstructed=[];

% Reconstruct phase and magnitude from real and imaginary part
if isempty(indexReal)~=1 
    if isempty(imIm)==0 && isempty(realIm)==0
        phaseReconstructed=atan2((imIm*slope(indexIm)+intercept(indexIm)),(realIm*slope(indexReal)+intercept(indexReal)));
        magnitudeReconstructed=sqrt(((realIm*slope(indexReal)+intercept(indexReal))).^2+((imIm*slope(indexIm)+intercept(indexIm))).^2);
    end
end

% Save everything in a structure
dataScan=struct('magnitudeReconstructed',magnitudeReconstructed,'phaseReconstructed',phaseReconstructed,...
    'matrixSize',matrixSize,'voxelSize',voxelSize,'TE',TE,'TI',TI,'CF',CF, 'B0_dir',B0_dir,'magn_rescaleFP',magnitude_rescaleFP,'im',im_rescaleFP,'real',real_rescaleFP,'Fat',fatIm,'Water',waterIm);



% save (filename, 'dataScan')
save(fullfile(folderToSave, filename),'dataScan')




