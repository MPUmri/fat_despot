%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright V�ronique Fortier 2020 - MIT License

% Mask for air and bone regions, for soft tissue regions and for the whole 
% object mask
                                                                   %
% ----------------------------------------------------------------------- %
% Input argument:
%   -magn_flyback (magnitude data at a short echo time to minimize
%   distorsion and signal loss
%   -matrixSize
% 
% Output argument:
%   -maskSoftTissue is a mask of the soft tissue, exlucing bone
%   -maskObject is a mask of the whole object
%   -maskAirBone is a mask of the air and bone regions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ maskSoftTissue, maskObject,maskAirBone] = inVivo_masks(magn_flyback, matrixSize)
%%

%this is not optimal. There are some spots in the eyes labelled as bone to be verified

% Histogram of all the pixels
figure()
h=histogram(magn_flyback(:,:,:,1)); %first echo
values=h.Values;
values=values(values~=0); 

vvalues=smooth(values);
peaks=findpeaks(vvalues);
peaks=max(peaks);

indice_second_max=find(vvalues==peaks);
vvalues=vvalues(1:indice_second_max);

indice_min_Histo=find(vvalues==min(vvalues));
if length(indice_min_Histo)>1
    indice_min_Histo=indice_min_Histo(1);
end

width=h.BinWidth; %width of the bins

% background threshold
background_threshold=indice_min_Histo*width;

% Define the soft tissue mask
maskSoftTissue=zeros(matrixSize);
maskSoftTissue(find(magn_flyback(:,:,:,1)>background_threshold))=1; 

% Define the contour mask by filling the soft tissue mask (3D)
maskObject=maskSoftTissue;
maskObject=imfill(maskObject,'holes');

%Morphological operations to ensure filling all air cavities %VF: before strel size=6 and 7 - for patientHN2 I had
%to increase because of dental prothesis (15 and 16)
se=strel('diamond',15);
maskObject=imdilate(maskObject,se);
maskObject=imfill(maskObject,'holes');
se=strel('disk',16);
maskObject=imerode(maskObject,se);
maskObject=imfill(maskObject,'holes');

%Define the air+bone mask
maskAirBone=(maskObject-maskSoftTissue);

maskObject(find(maskAirBone==-1))=1;
maskAirBone=(maskObject-maskSoftTissue);

%Remove small 'noise' regions from the mask with bwareaopen (the
%number that caracterize the opening needs to be tuned)
maskAirBone=bwareaopen(maskAirBone,20); %275 is too high for the head, but perfect for the leg
maskObject=bwareaopen(maskObject,26); 

%mask soft tissue
maskSoftTissue=(maskObject-maskAirBone);

% %This was added for sagittal head scan in case the head was close to the edge of the FOV
% if strcmp(anatomy,'head'==1)
%     maskAirBone(1:5,:,:)=0;
%     maskAirBone(:,1:5,:)=0;
% end;

%Remove the contour for the unwrapping step
maskObject(1,:,:)=0;
maskObject(:,1,:)=0;
maskObject(end,:,:)=0;
maskObject(:,end,:)=0;


maskAirBone=maskAirBone.*maskObject; 
maskSoftTissue=maskSoftTissue.*maskObject;

end


