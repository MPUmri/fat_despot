%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Véronique Fortier 2020 - MIT License
%
% B1 mapping - Dual angle B1 correction
%                                                                  
% Inputs:
%   -MagnData_2alpha: 2D multislice magnitude data at angle 2*alpha
%   -MagnData_alpha: 2D multislice magnitude data at angle alpha
%   Note: for Philips data, need to rescale magnitude data with the private
%   field 2005 100E (slope), intercept is 0.
%   -alpha: angle in degree (ex: 60) 
%   -maskObject_B1: mask of the object (logical 3D matrix)
%
% Outputs:
%   -B1_map_filtered: B1 map after gaussian filtering
%
% Based on: Boudreau et al. 2017, JMRI, DOI:10.1002/jmri.25692
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function [ B1_map_filtered ] = B1_mapping( MagnData_2alpha, MagnData_alpha, alpha, maskObject_B1 )
    
    alpha=degtorad(alpha);

    B1_map=(acos(MagnData_2alpha./(2*MagnData_alpha))/alpha);
    B1_map=B1_map.*maskObject_B1;
    outsideMask=logical(1-maskObject_B1);
    B1_map(outsideMask)=nan;
    B1_map(isinf(B1_map))=0;
    B1_map=abs(B1_map);
%     figure();imagesc((B1_map(:,:,ceil(matrixSizeB1(3)/2)).*maskObject_B1(:,:,ceil(matrixSizeB1(3)/2))));axis off;caxis([0.5 1.2]);colorbar;title('Unfiltered');colormap jet;set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

    % Smooth the B1 map
    hsize=[7 7];
    sigma=4.2466;
    h=fspecial('gaussian',hsize,sigma);
    clear B1_map_filtered
    for i=1:min(size(B1_map))
        a=nanconv(B1_map(:,:,i),h,'nanout');
        if isempty(a)==1
            B1_map_filtered(:,:,i)=zeros(matrixSizeB1(1),matrixSizeB1(2),matrixSizeB1(3));
        else
            B1_map_filtered(:,:,i)=a;
        end
    end
    B1_map_filtered(isnan(B1_map_filtered))=0;
    matrixSizeB1=size(B1_map_filtered);
    figure();imagesc((B1_map_filtered(:,:,ceil(matrixSizeB1(3)/2))).*maskObject_B1(:,:,ceil(matrixSizeB1(3)/2)));axis off;caxis([0.5 1.2]);colorbar;title('B_1 map filtered - Multi slice');colormap jet;set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');



end

