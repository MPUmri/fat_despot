%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Véronique Fortier 2020 - MIT License
%                                                                         %
% R2* mapping using log linear fit                                        %
%                                                                         %
% ------------------------------------------------------
% Inputs:
%   -Magnitude data with the 4th dimension corresponding to the TE
%   -TE
%   -Object mask
%   Which R2* mapping method to use, choose between log or weighted
%   -weighted_lls (0 or 1)
%   -log_lls (0 or 1) - This ishould be the most accurate
%
% Output:
%   -R2* map
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [ mapR2Star, mapR2Star_w,residual, weights_wlls_norm] = R2StarMapFast( MagnData, TE,maskHeadContour,weighted_lls,log_lls )
%%

% Define y, use masked vector to accelerate the process
for i=1:length(TE)   
    a=MagnData(:,:,:,i).*maskHeadContour(:,:,:);
    magnitudeMasked(:,i)=log(a(maskHeadContour(:,:,:)));
end

TE_reshape=repmat(TE,[max(size(magnitudeMasked)),1]);

% linear fit using matrix (much faster than voxel by voxel)
[a, b] = mylls(TE_reshape',magnitudeMasked');

a_reshape=repmat(a',[1,length(TE)]);

residual=(b'*TE+a_reshape-magnitudeMasked);

matrixSize=size(maskHeadContour);
mapR2Star=zeros(matrixSize);
mapR2Star(maskHeadContour)=-b;

% mapR2Star(find(mapR2Star<0))=0;
% mapR2Star(isinf(mapR2Star))=0;
% mapR2Star(isnan(mapR2Star))=0;

mapT2Star=1./mapR2Star;
mapT2Star(isinf(mapT2Star))=0;
mapT2Star(isnan(mapT2Star))=0;

% figure();montage(reshape4montage(mapR2Star.*maskHeadContour),[0 200]);
% figure();imagesc(mapR2Star(:,:,round(min(matrixSize)/2)));caxis([0 200]);axis off;title('Linear fit');colormap gray;colorbar;set(gcf,'color','w'); set(gca,'fontsize',20','fontweight','bold');

if weighted_lls==1
    % calculate the weights based on the standard deviation of the residual
    % at each echo
    std_residual=std(residual,0,1);
    weights_wlls=1./std_residual;
    weights_wlls_norm=weights_wlls/max(weights_wlls);
    
    % calculate the weights based on the interquartile range of the residual
    % at each echo
%     iqr_residual=iqr(residual);
%     weights_wlls=1./iqr_residual;
    
    
    
    weights_wlls_norm_reshape=repmat(weights_wlls_norm,[max(size(magnitudeMasked)),1]);
    [a_w, b_w] = mywlls(TE_reshape',magnitudeMasked',weights_wlls_norm_reshape');
    matrixSize=size(maskHeadContour);
    mapR2Star_w=zeros(matrixSize);
    mapR2Star_w(maskHeadContour)=-b_w;

%     mapR2Star_w(find(mapR2Star_w<0))=0;
%     mapR2Star_w(isinf(mapR2Star_w))=0;
%     mapR2Star_w(isnan(mapR2Star_w))=0;

    figure();montage(reshape4montage(mapR2Star_w.*maskHeadContour),[0 200]);
%     figure();imagesc(mapR2Star_w(:,:,round(min(matrixSize)/2)));caxis([0 200]);axis off;title('Weighted linear fit');colormap gray;colorbar;set(gcf,'color','w'); set(gca,'fontsize',20','fontweight','bold');
%     figure();imagesc(mapR2Star(:,:,round(min(matrixSize)/2))-mapR2Star_w(:,:,round(min(matrixSize)/2)));axis off;title('Linear fit - Weighted linear fit');colormap gray;colorbar;set(gcf,'color','w'); set(gca,'fontsize',20','fontweight','bold');
%     figure();montage(reshape4montage((mapR2Star-mapR2Star_w).*maskHeadContour),[-100 100]);
elseif log_lls==1
    weights_wlls_norm=1;
    % Define y, use masked vector to accelerate the process
    for i=1:length(TE)   
        a=MagnData(:,:,:,i).*maskHeadContour(:,:,:);
        magnitudeMasked(:,i)=(a(maskHeadContour(:,:,:)));
    end
    
    [a_w, b_w] = myloglls(TE_reshape',magnitudeMasked');
    mapR2Star_w=zeros(matrixSize);
    mapR2Star_w(maskHeadContour)=-b_w;

%     mapR2Star_w(find(mapR2Star_w<0))=0;
%     mapR2Star_w(isinf(mapR2Star_w))=0;
%     mapR2Star_w(isnan(mapR2Star_w))=0;
    
%     figure();imagesc(mapR2Star_w(:,:,round(min(matrixSize)/2)));caxis([0 200]);axis off;title('Log linear fit');colormap gray;colorbar;set(gcf,'color','w'); set(gca,'fontsize',20','fontweight','bold');
%     figure();imagesc(mapR2Star(:,:,round(min(matrixSize)/2))-mapR2Star_w(:,:,round(min(matrixSize)/2)));axis off;title('Linear fit - Log linear fit');colormap gray;colorbar;set(gcf,'color','w'); set(gca,'fontsize',20','fontweight','bold');


else
    weights_wlls_norm=1;
    mapR2Star_w=0;
end


end


