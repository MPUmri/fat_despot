function [a, b] = mywlls(x,y,w)
% function [a, b] = mywlls(x,y,w)
%
%   a function for fast weighted linear least squares regression of y = ones(size(x))*a + x*b
%
%   inputs:
%   x = x data arranged by column
%   y = y data arranged by column
%   w = weights arranged by column
%
%   outputs:
%   a = vector of intercepts
%   b = vector of slopes
%   

%
%   Ives Levesque, August 2012
%

n = size(x,1);
s = sum(w);

if size(y) ~= size(x)
    error('Size of x and y must match.')
end

if size(w) ~= size(x)
    error('Size of x and w must match.')
end

% estimate slope
num = sum(w.*x.*y) - sum(w.*x).*sum(w.*y)./s;
denom = sum(w.*x.^2) - (sum(w.*x).^2)./s;
b = num./denom;

% estimate intercept term
a = (sum(w.*y) - b.*sum(w.*x)) ./ s;