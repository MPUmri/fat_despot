function [a, b] = myloglls(x,y)
% function [a, b] = myloglls(x,y)
%
%   a function for fast linear least squares regression of log(y) = ones(size(x))*a + x*b
%	i.e. for linear fitting of the log of the function y = A exp(bx)
%	where a = log(A)
%
%	keep in mind that in Matlab, log is the natural logarithm (i.e. log(exp(x)) = x)
%
%   inputs:
%   x = x data arranged by column
%   y = y data arranged by column
%	the log(y) is taken inside the function
%
%   outputs:
%   a = vector of intercepts (linear fit)
%	--> to get A, the scale parameter, compute A = exp(a)
%   b = vector of slopes
%   

%
%   Ives Levesque, April 2018
%
%	ref: http://mathworld.wolfram.com/LeastSquaresFittingExponential.html
%

n = size(x,1);

if size(y) ~= size(x)
    error('Size of x and y must match.')
end

% estimate slope
% these were for standard LLS and left here for comparison
% num = sum(x.*y) - sum(x).*sum(y)/n;
% denom = sum(x.^2) - (sum(x).^2)/n;

ly = log(y);
num = sum(y).*sum(x.*y.*ly) - sum(x.*y).*sum(y.*ly);
denom = sum(y).*sum(y.*(x.^2)) - (sum(x.*y).^2);

b = num./denom;

% estimate intercept term
numa = sum(y.*(x.^2)).*sum(y.*ly) - sum(x.*y).*sum(x.*y.*ly);
denoma = sum(y).*sum(y.*(x.^2)) - (sum(x.*y).^2);
a = numa./denoma;