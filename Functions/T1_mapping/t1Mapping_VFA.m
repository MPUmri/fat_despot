%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Véronique Fortier 2020 - MIT License
%
% T1 mapping - Variable flip angle technique, linear fit                  %
%
% Inputs:
%  -magn_FA = magnitude images, 4th dim is FA (in rad), choose an echo time..
% Note: for Philips data, need to rescale magnitude data with the private
% field 2005 100E (slope), intercept is 0.
%  -maskHead = tissue mask
%  -FA = Flip angle values as a vector
%  -matrixSize = size of the matrix

% Outputs:
%  -T1_map
%  -M0_map
% Based on: Wang et al. Rapid 3D-T1 Mapping of Cartilage With Variable 
% Flip Angle and Parallel Imaging at 3.0T. (2008). JMRI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [ T1_map, M0_map ] = t1Mapping_VFA( magn_FA, maskHead_T1, TR,FA,matrixSize,mapFA )


    % maskHead_T1=logical(maskHeadContour(:,:,matrixSize(3)/2));
    % magn_FA_norm=magn_FA/(max(max(max(max(magn_FA)))));

    % Define x and y, use masked vector to accelerate the process
    for i=1:length(FA)

        a=magn_FA(:,:,:,i).*maskHead_T1;
        b=mapFA(:,:,:,i);
        x(i,:)=a(maskHead_T1)./tan(b(maskHead_T1));
        y(i,:)=a(maskHead_T1)./sin(b(maskHead_T1));
    end

    % Linear fit over all voxels in the mask this will take around 30 min to
    % perform
    for i=1:max(size(x))
        p(:,:,i)=polyfit(x(:,i),y(:,i),1);
    end

    m=p(1,1,:);
    m(find(m<0))=0;
    b=p(1,2,:);
    % matrixSize(3)=1;
    m_map=zeros(matrixSize(1), matrixSize(2), matrixSize(3));
    b_map=zeros(matrixSize(1), matrixSize(2), matrixSize(3));
    b_map(maskHead_T1)=b;
    m_map(maskHead_T1)=m;

    T1_map=zeros(matrixSize(1), matrixSize(2), matrixSize(3));
    M0_map=zeros(matrixSize(1), matrixSize(2), matrixSize(3));
    a=-TR./log((m(:)));
    T1_map(maskHead_T1)=a(:);
    T1_map(find(T1_map<0))=0;
    M0_map(maskHead_T1)=b./(1-m);

    % figure();imagesc((M0_map.*maskHead_T1));colormap gray;colorbar;caxis([0 5000000]);axis off;set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

    % Plot the fit
%     x1=(0:1:(max(x(:,i))));
%     y1=polyval(p(:,:,i),(x1));
    % figure();plot(x(:,i),y(:,i),' o','linewidth',2,'markersize',8);hold on;plot(x1,y1,'--k','linewidth',2); legend('Measurements','Fit');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');grid off
    % xlabel('Signal/tan(FA)');ylabel('Signal/sin(FA)')


end

