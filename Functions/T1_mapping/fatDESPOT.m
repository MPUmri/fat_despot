%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Véronique Fortier 2020 - MIT License
%
% Fat DESPOT - 2-compartment T1 mapping with single R2* fit               %
% The implementation is currently in 2D, can be easily expanded to 3D
%                                                                         %
% Implementation based on: Le Ster et al. 2016, JMRI, DOI:10.1002/jmri.25205
% ------------------------------------------------------
% Inputs:
%   -Magnitude data with the 4th dimension corresponding to the TE and the
%   fifth dimension corresponding to the FA
%   -TE (in s)
%   -TR (in s)
%   -FA (in degree)
%   -dfat (fat spectrum in Hz)
%   -rel amp (relative amplitude of each fat peak, the sum should be 1)
%   -B1 map
%   -mask (object mask)
%   -FF (fat fraction map in %)
%   -R2* map (in s-1)
%   -T1 map in s avec B1 correction
%
%
% Outputs:
%   -fat_frac: Fat fraction map
%   -t1w_2: Water T1 map
%   -t1f_2: Fat T1 map
%   -r_squared: correlation coefficient of the fit
%   -residuals of the fit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 



function [ fat_frac, t1w_2, t1f_2, r_squared,  residual] = fatDESPOT( magn_FA_fw, TE, TR, FA,dfat,relAmp,B1_map, mask,FF,mapR2Star,mapT1, TE_all1,TE_all2)

    mask=logical(mask);

    % Reshape FA and TE in matrix form
    FA=degtorad(FA);
    for i=1:length(FA)
       mapFA(:,i)=ones(length(TE),1).*FA(i);
    end    
    TE_reshape_vector=repmat(TE',[length(FA),1]);
   
    % Reshape the measurements (magnitude signal) in a vector. Use only
    % voxels within the mask to reduce computation time
    for i=1:length(TE)
        for j=1:length(FA)   
            a=magn_FA_fw(:,:,:,i,j);
            magn_FA_fw_masked(:,i,j)=a(mask);
        end
    end

    % Reshape the intial guesses matrix in a vector. Use only
    % voxels within the mask to reduce computation time
    B1_map_filtered_reshape_all=B1_map(mask);
    T1_map_B1corr_reshape_all=mapT1(mask);
    R2StarMap_reshape_masked=mapR2Star(mask);
    FF_masked=FF(mask);
    
    % Fat spectrum term (cos)
    fatSpectrum_cos=zeros(length(TE),1);
    for i=1:length(TE)
        c=0;
        for j=1:length(dfat)   
            c = c+(relAmp(j)*cos(2*pi*TE(i)*dfat(j)));
        end
        fatSpectrum_cos(i) = (c);
    end
    fatSpectrum_cos=repmat(fatSpectrum_cos,[length(FA),1]);

    % Fat spectrum term (sin)
    fatSpectrum_sin=zeros(length(TE),1);
    for i=1:length(TE)
        c=0;
        for j=1:length(dfat)   
            c = c+(relAmp(j)*sin(2*pi*TE(i)*dfat(j)));
        end
        fatSpectrum_sin(i) = (c);
    end
    fatSpectrum_sin=repmat(fatSpectrum_sin,[length(FA),1]);

    % Create waitbar
    h=waitbar(0,'Please wait...');
    steps=(size(magn_FA_fw_masked,1));
    clear x residual resnorm r_squared
    n=1;

    % Perform the fit voxel by voxel
    for k=1:steps

        magn_FA_fw_reshape=magn_FA_fw_masked(k,:,:);
        magn_FA_fw_reshape_vector=magn_FA_fw_reshape(:);
        
        B1_map_filtered_reshape=B1_map_filtered_reshape_all(k);        
        T1_map_B1corr_reshape=T1_map_B1corr_reshape_all(k);  

        R2StarMap_reshape=R2StarMap_reshape_masked(k);

        FF_reshape=FF_masked(k);

        % B1 correction
        for i=1:length(FA)
            mapFA_scale(:,i)=mapFA(:,i).*abs(B1_map_filtered_reshape);
        end
        mapFA_scale_vector=mapFA_scale(:);

        % Signal model used (1 R2*, magnitude only)
        signalModeling_magnitude_single_R2star=@(y) magn_FA_fw_reshape_vector - (y(1).*...
            sqrt(((1-y(4)/100).*exp(-TE_reshape_vector.*y(5)).*((((1-exp(-TR.*y(2))).*sin(mapFA_scale_vector))./...
            (1-exp(-TR.*y(2)).*cos(mapFA_scale_vector))))+...
            ((y(4)/100).*exp(-TE_reshape_vector.*y(5)).*((((1-exp(-TR.*y(3))).*sin(mapFA_scale_vector))./(1-exp(-TR.*y(3)).*...
            cos(mapFA_scale_vector)))).*fatSpectrum_cos)).^2+(...
            ((y(4)/100).*exp(-TE_reshape_vector.*y(5)).*((((1-exp(-TR.*y(3))).*sin(mapFA_scale_vector))./(1-exp(-TR.*y(3)).*...
            cos(mapFA_scale_vector)))).*fatSpectrum_sin)).^2));

        % Define initial guess and upper/lower bounds
        x0=ones(1,5);
        lb=ones(1,5);
        ub=ones(1,5);

        x0(1,1)=max(magn_FA_fw_reshape_vector(:))*10;
        R1_map_B1corr_reshape=1./T1_map_B1corr_reshape;
        R1_map_B1corr_reshape(isnan(R1_map_B1corr_reshape))=1;
        if FF_reshape<50
            x0(1,2)=(R1_map_B1corr_reshape);
            x0(1,3)=4;
        else
            x0(1,3)=(R1_map_B1corr_reshape);
            x0(1,2)=1/1;
        end
        x0(1,4)=FF_reshape;
        x0(1,5)=(R2StarMap_reshape);

        lb(1,1)=0.00001;
        lb(1,2)=1/3;
        lb(1,3)=1/1;
        lb(1,4)=0.01;
        lb(1,5)=1/0.8;

        ub(1,1)=1*10^(15);
        ub(1,2)=1/0.2;
        ub(1,3)=1/0.1;
        ub(1,4)=99.99;
        ub(1,5)=1000;

        % Non linear fit
        options_fit = optimoptions(@lsqnonlin,'Algorithm','trust-region-reflective','Display','off');
        [x(k,:),resnorm(k,:),residual(:,k) ]= lsqnonlin(signalModeling_magnitude_single_R2star,x0,lb,ub,options_fit);

        % Calculate the correlation coefficient
        SSE=sum(residual(:,k).^2);
        y_mean=mean(magn_FA_fw_reshape_vector);
        SST=sum((magn_FA_fw_reshape_vector-y_mean).^2);
        r_squared(k)=1-SSE/SST;

        % Update waitbar
        if k==n*round(steps/(10))
            n=n+1;
            waitbar(k/steps)
        end
    end
    
    % Close waitbar
    close(h)

    
    %% Display results
    sliceShow=1;
    matrixSize=size(B1_map);
    fat_frac=zeros(matrixSize(1),matrixSize(2), matrixSize(3));
    fat_frac(mask)=(x(:,4));
    figure();imagesc((fat_frac(:,:,sliceShow)));colorbar;colormap gray;axis off;caxis([0 35]);title('Fat fraction - Joint fit [%]');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

    t1w=zeros(matrixSize(1),matrixSize(2), matrixSize(3));
    t1w(mask)=1./(x(:,2));
    t1w(isnan(t1w))=0;
    t1w_2=t1w;
    t1w_2(find(fat_frac>99))=nan;
    t1w_2(find(round(t1w_2,3)==(1/lb(1,2))))=nan;
    t1w_2(find(round(t1w_2,3)==(1/ub(1,2))))=nan;
    figure();imagesc(((t1w_2(:,:,sliceShow)*1000)).*mask(:,:,sliceShow));colorbar;colormap gray;axis off;title('Water T_1 [ms]');caxis([0 1500]);set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

    t1f=zeros(matrixSize(1),matrixSize(2), matrixSize(3));
    t1f(mask)=1./(x(:,3));
    t1f(isnan(t1f))=0;
    t1f_2=t1f;
    t1f_2(find(fat_frac<4))=nan;
    figure();imagesc(((t1f_2(:,:,sliceShow)).*1000));colorbar;colormap gray;axis off;title('Fat T_1  [ms]');caxis([0 800]);set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

%     S0=zeros(matrixSize(1),matrixSize(2), matrixSize(3));
%     S0(mask)=x(:,1);
    % figure();imagesc((S0(:,:,sliceShow)).*mask(:,:,sliceShow));colorbar;colormap gray;caxis([0 60000000]);axis off;title('S0');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');


%     R2Star=zeros(matrixSize(1),matrixSize(2), matrixSize(3));
%     R2Star(mask)=x(:,5);
%     figure();imagesc(R2Star(:,:,sliceShow));colorbar;colormap jet;axis off;caxis([0 50]);title('Water R_2* fit');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

%     residualsMap=zeros(matrixSize(1),matrixSize(2), matrixSize(3));
%     residualsMap(mask)=sqrt(sum(residual.^2)/min(size(residual)));
    % figure();imagesc(residualsMap(:,:,sliceShow));caxis([0 50000]);colorbar;colormap jet;axis off;title('RMSE');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');

    r_squared_Map=zeros(matrixSize(1),matrixSize(2), matrixSize(3));
    r_squared_Map(mask)=r_squared;
    figure();imagesc(r_squared_Map(:,:,sliceShow));caxis([0.3 1]);colorbar;colormap jet;axis off;title('R^2');set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');



    %% Show fit quality as a function of flip angle and TEs
    delta_TE=TE(2)-TE(1);
    TE_sim=[0.001:0.00002:(TE(end)+delta_TE)];
    TE_reshape_vector=repmat(TE_sim',[length(FA),1]);

    fatSpectrum_cos=zeros(length(TE_sim),1);
    for i=1:length(TE_sim)
        c=0;
        for j=1:length(dfat)   
            c = c+(relAmp(j)*cos(2*pi*TE_sim(i)*dfat(j)));
        end
        fatSpectrum_cos(i) = (c);
    end
    fatSpectrum_cos=repmat(fatSpectrum_cos,[length(FA),1]);

    fatSpectrum_sin=zeros(length(TE_sim),1);
    for i=1:length(TE_sim)
        c=0;
        for j=1:length(dfat)   
            c = c+(relAmp(j)*sin(2*pi*TE_sim(i)*dfat(j)));
        end
        fatSpectrum_sin(i) = (c);
    end
    fatSpectrum_sin=repmat(fatSpectrum_sin,[length(FA),1]);


    clear mapFA mapFA_scale
    for i=1:length(FA)
       mapFA(:,i)=ones(length(TE_sim),1).*FA(i);
    end

    for i=1:length(FA)
       mapFA_scale(:,i)=mapFA(:,i).*abs(B1_map_filtered_reshape);
    end
    mapFA_scale_vector=mapFA_scale(:);

    % k=10;

    magn_FA_fw_reshape=magn_FA_fw_masked(k,:,:);
    magn_FA_fw_reshape_vector=magn_FA_fw_reshape(:);

    B1_map_filtered_reshape=B1_map_filtered_reshape_all(k);        
    T1_map_B1corr_reshape=T1_map_B1corr_reshape_all(k);  

    R2StarMap_reshape=R2StarMap_reshape_masked(k);

    f1_reshape=FF_masked(k);

    y=x(k,:);
    signal_sim=(y(1).*...
            sqrt(((1-y(4)/100).*exp(-TE_reshape_vector.*y(5)).*((((1-exp(-TR.*y(2))).*sin(mapFA_scale_vector))./...
            (1-exp(-TR.*y(2)).*cos(mapFA_scale_vector))))+...
            ((y(4)/100).*exp(-TE_reshape_vector.*y(5)).*((((1-exp(-TR.*y(3))).*sin(mapFA_scale_vector))./(1-exp(-TR.*y(3)).*...
            cos(mapFA_scale_vector)))).*fatSpectrum_cos)).^2+(...
            ((y(4)/100).*exp(-TE_reshape_vector.*y(5)).*((((1-exp(-TR.*y(3))).*sin(mapFA_scale_vector))./(1-exp(-TR.*y(3)).*...
            cos(mapFA_scale_vector)))).*fatSpectrum_sin)).^2));

    figure();hold on;
    plot(TE_sim*1000, signal_sim(1:length(TE_sim)),'b--','linewidth',2)
    plot(TE_sim*1000, signal_sim((length(TE_sim)+1):2*length(TE_sim)),'r--','linewidth',2)
    if length(FA)==4
        plot(TE_sim*1000, signal_sim((2*length(TE_sim)+1):3*length(TE_sim)),'k--','linewidth',2)
        plot(TE_sim*1000, signal_sim((3*length(TE_sim)+1):4*length(TE_sim)),'g--','linewidth',2)
    end 

    plot(TE_all1*1000, magn_FA_fw_reshape_vector(1:2:length(TE)),' bo','linewidth',2,'markersize',8,'markerfacecolor','b')
    plot(TE_all1*1000, magn_FA_fw_reshape_vector((length(TE)+1):2:2*length(TE)),' ro','linewidth',2,'markersize',8,'markerfacecolor','r')
    if length(FA)==4
        plot(TE_all1*1000, magn_FA_fw_reshape_vector((2*length(TE)+1):2:3*length(TE)),' ko','linewidth',2,'markersize',8,'markerfacecolor','k')
        plot(TE_all1*1000, magn_FA_fw_reshape_vector((3*length(TE)+1):2:4*length(TE)),' go','linewidth',2,'markersize',8,'markerfacecolor','g')
    end

    plot(TE_all2*1000, magn_FA_fw_reshape_vector(2:2:length(TE)),' bx','linewidth',2,'markersize',8,'markerfacecolor','b')
    plot(TE_all2*1000, magn_FA_fw_reshape_vector((length(TE)+2):2:2*length(TE)),' rx','linewidth',2,'markersize',8,'markerfacecolor','r')
    if length(FA)==4
        plot(TE_all2*1000, magn_FA_fw_reshape_vector((2*length(TE)+2):2:3*length(TE)),' kx','linewidth',2,'markersize',8,'markerfacecolor','k')
        plot(TE_all2*1000, magn_FA_fw_reshape_vector((3*length(TE)+2):2:4*length(TE)),' gx','linewidth',2,'markersize',8,'markerfacecolor','g')
    end

     set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');grid on
     xlabel('TE [ms]')
     ylabel('Signal [a.u.]')
     legend('FA=3','FA=6','FA=15','FA=34')
     title('1-voxel fit quality')

end
