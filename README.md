Fat DESPOT: Two-compartment T1 fit
Copyright Véronique Fortier 2023 - MIT License 
_______________________________________________


Run ‘FatDESPOT_main.m’ to perform the T1 fit

Run ‘simulationFatDESPOT_main.m’  to perform Fat DESPOT simulations

——————————————————

Refer to the publication for more details on the experiments: Fortier, V., & Levesque, I. R. (2023). MR-oximetry with fat DESPOT. Magnetic Resonance Imaging.

——————————————————

MRI data (Dicom) and raw MRS data (.mat format) are available at: https://osf.io/gzajw/

——————————————————

*For the 3-point Dixon fat/water separation (note from the ISMRM toolbox):

A subroutine of the algorithm is implemented in c++ for efficiency. The c++ file (RG.cpp) must be compiled before MATLAB can use it. This can be done in MATLAB by navigating to the source code folder, then typing in the MATLAB command window:
 
    mex RG.cpp
 
If this doesn't work, you may have to specify a compiler by typing: mex -setup

